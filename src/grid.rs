use bevy::core::FixedTimestep;
use bevy::math::vec3;
use bevy::prelude::*;
use bevy::sprite::Anchor;

use crate::mouse_tracker::{MouseClicked, MousePosition};
use crate::ui::{UIEvent, UIEvtClass};

// Entity
#[derive(Component)]
struct Cell {
    state: CellState,
}

enum CellState {
    Alive,
    Dead,
    Empty,
}

#[derive(Component, Clone, Copy)]
struct Position(Vec2);

//Simulation resource
pub struct Simulation {
    pub state: SimState,
}
pub enum SimState {
    Running,
    Stopped,
}

//Maybe move this into a resource and just read it from there.
const INIT_SIZE: f32 = 20.;
const GRID_WIDTH: u32 = 150;
const GRID_HEIGHT: u32 = 100;

//Stage for the system
#[derive(Debug, Hash, PartialEq, Eq, Clone, StageLabel)]
struct CellUpdateStage;

//Plugin implementation
#[derive(Default)]
pub struct GridPlugin;

impl Plugin for GridPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup_grid)
            .insert_resource(Simulation {
                state: SimState::Stopped,
            })
            .add_system(sim_controls)
            .add_system(cell_interaction)
            .add_system(cell_hover)
            .add_system(clear_board)
            .add_stage(
                CellUpdateStage,
                SystemStage::parallel()
                    .with_run_criteria(
                        FixedTimestep::step(0.05)
                            // labels are optional. they provide a way to access the current
                            // FixedTimestep state from within a system
                            .with_label("cell_update_timestep"),
                    )
                    .with_system(sim_step),
            );
    }
}

fn setup_grid(mut commands: Commands) {
    for x in 0..GRID_WIDTH {
        for y in 0..GRID_HEIGHT {
            let sq_pos = get_pos_from_coord((x, y));
            commands
                .spawn_bundle(SpriteBundle {
                    transform: Transform {
                        translation: Vec3::new(sq_pos.0, sq_pos.1, 0.),
                        scale: vec3(1., 1., 0.),
                        ..default()
                    },
                    sprite: Sprite {
                        color: Color::BLACK,
                        custom_size: Some(Vec2::splat(INIT_SIZE)),
                        anchor: Anchor::BottomLeft,
                        ..default()
                    },
                    ..default()
                })
                .insert(Position(Vec2::from(sq_pos)))
                .insert(Cell {
                    state: CellState::Empty,
                });
        }
    }
}

fn clear_board(
    mut ui_evt: EventReader<UIEvent>,
    mut cells: Query<(&mut Cell, &mut Sprite)>,
    mut simulation: ResMut<Simulation>,
) {
    for evt in ui_evt.iter() {
        match evt.0 {
            UIEvtClass::Clear => {
                for (mut cell, mut sprite) in cells.iter_mut() {
                    cell.state = CellState::Empty;
                    sprite.color = cell_color(&cell);
                }
            }
            UIEvtClass::SimStartStop => {
                simulation.state = match simulation.state {
                    SimState::Running => SimState::Stopped,
                    SimState::Stopped => SimState::Running,
                }
            } // _ => (),
        }
    }
}

fn sim_controls(mut simulation: ResMut<Simulation>, kb_input: Res<Input<KeyCode>>) {
    if kb_input.just_released(KeyCode::P) {
        simulation.state = match simulation.state {
            SimState::Running => SimState::Stopped,
            SimState::Stopped => SimState::Running,
        }
    }
}

fn cell_interaction(
    mut cells: Query<(&Position, &mut Cell, &mut Sprite)>,
    mut mouse_events: EventReader<MouseClicked>,
    mut ui_evt: EventReader<UIEvent>,
) {
    //Need a more reliable way to make sure there are no UI events going on. Cells under the buttons receive the interaction.
    if !ui_evt.iter().next().is_some() {
        for mouse_ev in mouse_events.iter() {
            cells.for_each_mut(|(&position, mut cell, mut sprite)| {
                if is_in_cell_bounds(mouse_ev.0, position.0) {
                    cell.state = match cell.state {
                        CellState::Alive => CellState::Empty,
                        CellState::Empty | CellState::Dead => CellState::Alive,
                    };
                    sprite.color = cell_color(&cell);
                }
            });
        }
    }
}

fn cell_hover(
    mut cells: Query<(&Position, &Cell, &mut Sprite)>,
    mouse_position: Res<MousePosition>,
) {
    if mouse_position.is_changed() {
        cells.for_each_mut(|(&position, cell, mut sprite)| {
            if is_in_cell_bounds(mouse_position.0, position.0) {
                sprite.color = Color::WHITE;
            } else {
                sprite.color = cell_color(&cell);
            }
        });
    }
}

fn sim_step(simulation: Res<Simulation>, mut cells: Query<(&Position, &mut Cell, &mut Sprite)>) {
    if let SimState::Running = simulation.state {
        let mut census: Vec<bool> = Vec::new();

        for (_, cell, _) in cells.iter() {
            census.push(match cell.state {
                CellState::Alive => true,
                CellState::Empty | CellState::Dead => false,
            })
        }

        for (idx, (_, mut cell, mut sprite)) in cells.iter_mut().enumerate() {
            let index = idx.clone() as i32;
            let col_height = GRID_HEIGHT as i32;
            let mut neighbor_count: usize = 0;

            //Neighboors indexes from bottom left:
            let neighboors = vec![
                index - col_height + 1,
                index - col_height,
                index - col_height - 1,
                index + 1,
                index + col_height + 1,
                index + col_height,
                index + col_height - 1,
                index - 1,
            ];

            //Match neighbors to the life census to count the living.
            for neigh_idx in neighboors {
                if neigh_idx >= 0 && neigh_idx < census.len() as i32 {
                    if census[neigh_idx as usize] {
                        neighbor_count += 1;
                    }
                }
            }

            if neighbor_count < 2 || neighbor_count > 3 {
                match cell.state {
                    CellState::Alive => {
                        cell.state = CellState::Dead;
                    }
                    CellState::Dead | CellState::Empty => {}
                }
            }

            if neighbor_count == 3 {
                cell.state = CellState::Alive;
            }

            sprite.color = cell_color(&cell);
        }
    }
}

// region: Helpers
fn cell_color(cell: &Cell) -> Color {
    match cell.state {
        CellState::Alive => Color::GREEN,
        CellState::Empty => Color::BLACK,
        CellState::Dead => Color::GRAY,
    }
}

fn get_pos_from_coord(coord: (u32, u32)) -> (f32, f32) {
    (
        (coord.0 as f32 - GRID_WIDTH as f32 / 2.) * INIT_SIZE,
        (coord.1 as f32 - GRID_HEIGHT as f32 / 2.) * INIT_SIZE,
    )
}

fn is_in_cell_bounds(mouse_pos: Vec2, cell_pos: Vec2) -> bool {
    mouse_pos.x > cell_pos.x
        && mouse_pos.x < cell_pos.x + INIT_SIZE
        && mouse_pos.y > cell_pos.y
        && mouse_pos.y < cell_pos.y + INIT_SIZE
}

/* fn animate_scale(tim: Timer) {
    todo!("Implement animation for scaling hovered cells.")
} */
// endregion: Helpers
