use bevy::core::FixedTimestep;
use bevy::prelude::*;
use bevy::render::camera::RenderTarget;

use crate::camera::MainCamera;

//Events
pub struct MouseClicked(pub Vec2);

//Mouse position resource
pub struct MousePosition(pub Vec2);

//Stage for the system
#[derive(Debug, Hash, PartialEq, Eq, Clone, StageLabel)]
struct CellUpdateStage;

//Plugin
#[derive(Default)]
pub struct MouseTrackerPlugin;

impl Plugin for MouseTrackerPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<MouseClicked>()
            .insert_resource(MousePosition(Vec2::new(0., 0.)))
            .add_system(mouse_click)
            .add_stage(
                CellUpdateStage,
                SystemStage::parallel()
                    .with_run_criteria(
                        FixedTimestep::step(0.016)
                            // labels are optional. they provide a way to access the current
                            // FixedTimestep state from within a system
                            .with_label("cell_update_timestep"),
                    )
                    .with_system(cursor_position),
            );
    }
}

fn cursor_position(
    // need to get window dimensions
    wnds: Res<Windows>,
    mut mouse_moved: EventReader<CursorMoved>,
    mut mouse_pos: ResMut<MousePosition>,
    // query to get camera transform
    q_camera: Query<(&Camera, &GlobalTransform), With<MainCamera>>,
) {
    if mouse_moved.iter().next().is_some() {
        // get the camera info and transform
        // assuming there is exactly one main camera entity, so query::single() is OK
        let (camera, camera_transform) = q_camera.single();

        // get the window that the camera is displaying to (or the primary window)
        let wnd = if let RenderTarget::Window(id) = camera.target {
            wnds.get(id).unwrap()
        } else {
            wnds.get_primary().unwrap()
        };

        // check if the cursor is inside the window and get its position

        if let Some(screen_pos) = wnd.cursor_position() {
            // get the size of the window
            let window_size = Vec2::new(wnd.width() as f32, wnd.height() as f32);

            // convert screen position [0..resolution] to ndc [-1..1] (gpu coordinates)
            let ndc = (screen_pos / window_size) * 2.0 - Vec2::ONE;

            // matrix for undoing the projection and camera transform
            let ndc_to_world =
                camera_transform.compute_matrix() * camera.projection_matrix.inverse();

            // use it to convert ndc to world-space coordinates
            let world_pos = ndc_to_world.project_point3(ndc.extend(-1.0));

            // reduce it to a 2D value
            let world_pos: Vec2 = world_pos.truncate();

            mouse_pos.0 = Vec2::from((world_pos.x, world_pos.y));
        }
    }
}

fn mouse_click(
    mut mouse_clicked_evt: EventWriter<MouseClicked>,
    mouse_btn: Res<Input<MouseButton>>,
    mouse_pos: Res<MousePosition>,
) {
    if mouse_btn.just_pressed(MouseButton::Left) {
        mouse_clicked_evt.send(MouseClicked(Vec2::from((mouse_pos.0.x, mouse_pos.0.y))));
    }
}
