use bevy::{
    app::AppExit,
    diagnostic::{Diagnostics, FrameTimeDiagnosticsPlugin},
    prelude::*,
};

use crate::grid::{SimState, Simulation};

//Events
pub struct UIEvent(pub UIEvtClass);

pub enum UIEvtClass {
    Clear,
    SimStartStop,
}

//Entities
#[derive(Component)]
struct UiText;

#[derive(Component)]
struct DebugText;

#[derive(Component)]
struct UIButton(UIButtonType);

#[derive(PartialEq, Eq)]
enum UIButtonType {
    Clear,
    SimControl,
    Exit,
}

#[derive(Component)]
struct SimStatusIndicator;

//Plugin Implementation
#[derive(Default)]
pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<UIEvent>()
            .add_startup_system(setup)
            .add_plugin(FrameTimeDiagnosticsPlugin)
            .add_system(debug_fps_system)
            .add_system(ui_evt_dispatcher)
            .add_system(ui_btn_hover)
            .add_system(sim_control_btn_system);
    }
}

//Startup system
fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    let font = asset_server.load("TitilliumWeb-SemiBold.ttf");
    let text_style: TextStyle = TextStyle {
        font: font.clone(),
        font_size: 24.0,
        color: Color::BLACK,
        ..default()
    };

    commands.spawn_bundle(UiCameraBundle::default());

    commands
        .spawn_bundle(NodeBundle {
            style: Style {
                size: Size::new(Val::Percent(100.0), Val::Px(50.0)),
                justify_content: JustifyContent::SpaceBetween,
                align_items: AlignItems::Center,
                position_type: PositionType::Absolute,
                position: Rect {
                    top: Val::Px(0.),
                    ..default()
                },
                padding: Rect {
                    left: Val::Percent(5.),
                    right: Val::Percent(5.),
                    ..default()
                },
                ..default()
            },
            color: UiColor::from(Color::WHITE),
            ..Default::default()
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(TextBundle {
                    style: Style { ..default() },
                    text: Text::with_section(
                        "Conway's Game Of Life",
                        text_style.clone(),
                        Default::default(),
                    ),
                    ..default()
                })
                .insert(UiText);

            //FPS counter.
            parent
                .spawn_bundle(NodeBundle {
                    style: Style {
                        align_items: AlignItems::Center,
                        justify_content: JustifyContent::Center,
                        size: Size::new(Val::Px(80.), Val::Percent(100.)),
                        ..default()
                    },
                    color: Color::WHITE.into(),
                    ..default()
                })
                .with_children(|parent| {
                    parent
                        .spawn_bundle(TextBundle {
                            text: Text::with_section(
                                "FPS: ",
                                TextStyle {
                                    color: Color::BLACK.into(),
                                    ..text_style.clone()
                                },
                                Default::default(),
                            ),
                            ..default()
                        })
                        .insert(DebugText);
                });
        });

    //Clear board button.
    commands
        .spawn_bundle(build_classic_button(1, None))
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text::with_section(
                    "Clear",
                    TextStyle {
                        color: Color::WHITE.into(),
                        ..text_style.clone()
                    },
                    Default::default(),
                ),
                ..default()
            });
        })
        .insert(UIButton(UIButtonType::Clear));

    //Start/Stop simulation button.
    commands
        .spawn_bundle(build_classic_button(2, Some(Color::GREEN)))
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text::with_section(
                    "Start",
                    TextStyle {
                        color: Color::BLACK.into(),
                        ..text_style.clone()
                    },
                    Default::default(),
                ),
                ..default()
            });
        })
        .insert(UIButton(UIButtonType::SimControl));

    //Exit button.
    #[cfg(not(target_family = "wasm"))]
    commands
        .spawn_bundle(build_classic_button(3, None))
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text::with_section(
                    "Exit",
                    TextStyle {
                        color: Color::BLACK.into(),
                        ..text_style.clone()
                    },
                    Default::default(),
                ),
                ..default()
            });
        })
        .insert(UIButton(UIButtonType::Exit));
}

//Systems
fn debug_fps_system(
    diagnostics: Res<Diagnostics>,
    mut text_query: Query<&mut Text, With<DebugText>>,
) {
    let mut text = text_query.single_mut();
    let mut fps = 0.0;

    if let Some(fps_diagnostic) = diagnostics.get(FrameTimeDiagnosticsPlugin::FPS) {
        if let Some(fps_avg) = fps_diagnostic.average() {
            fps = fps_avg;
        }
    }

    text.sections[0].value = format!("FPS: {:.1}", fps);
}

fn ui_evt_dispatcher(
    mut ui_event: EventWriter<UIEvent>,
    mut app_exit_event: EventWriter<AppExit>,
    mut button_query: Query<(&Interaction, &UIButton), (Changed<Interaction>, With<Button>)>,
) {
    button_query.for_each_mut(|(btn_interaction, btn_type)| match btn_interaction {
        Interaction::Clicked => match btn_type.0 {
            UIButtonType::Clear => {
                ui_event.send(UIEvent(UIEvtClass::Clear));
            }
            UIButtonType::SimControl => {
                ui_event.send(UIEvent(UIEvtClass::SimStartStop));
            }
            UIButtonType::Exit => {
                app_exit_event.send_default();
            }
        },
        _ => (),
    })
}

fn ui_btn_hover(
    mut button_query: Query<
        (&Interaction, &mut UiColor, &Children, &UIButton),
        (Changed<Interaction>, With<Button>),
    >,
    sim_state: Res<Simulation>,
    mut btn_text_query: Query<&mut Text>,
) {
    button_query.for_each_mut(|(btn_interaction, mut btn_color, btn_children, btn_type)| {
        match btn_interaction {
            Interaction::None => match btn_type.0 {
                UIButtonType::Clear | UIButtonType::Exit => {
                    *btn_color = Color::GRAY.into();
                    for btn_child in btn_children.iter() {
                        let mut btn_text = btn_text_query.get_mut(*btn_child).unwrap();
                        btn_text.sections[0].style.color = Color::WHITE.into();
                    }
                }
                UIButtonType::SimControl => {
                    *btn_color = if let SimState::Running = sim_state.state {
                        Color::RED.into()
                    } else {
                        Color::GREEN.into()
                    };
                    for btn_child in btn_children.iter() {
                        let mut btn_text = btn_text_query.get_mut(*btn_child).unwrap();
                        btn_text.sections[0].style.color = Color::BLACK.into();
                    }
                }
            },
            Interaction::Hovered => {
                *btn_color = Color::WHITE.into();
                for btn_child in btn_children.iter() {
                    let mut btn_text = btn_text_query.get_mut(*btn_child).unwrap();
                    btn_text.sections[0].style.color = Color::GRAY.into();
                }
            }
            _ => (),
        }
    })
}

fn sim_control_btn_system(
    mut button_query: Query<(&Children, &UIButton, &mut UiColor), With<UIButton>>,
    sim_state: Res<Simulation>,
    mut btn_text_query: Query<&mut Text>,
) {
    if sim_state.is_changed() {
        for (btn_children, btn_type, mut btn_color) in button_query.iter_mut() {
            if let UIButtonType::SimControl = btn_type.0 {
                for btn_child in btn_children.iter() {
                    let mut btn_text = btn_text_query.get_mut(*btn_child).unwrap();
                    match sim_state.state {
                        SimState::Running => {
                            btn_text.sections[0].value = "Stop".to_owned();
                            *btn_color = Color::RED.into();
                        }
                        SimState::Stopped => {
                            btn_text.sections[0].value = "Start".to_owned();
                            *btn_color = Color::GREEN.into();
                        }
                    }
                }
            }
        }
    }
}

//Helpers
fn build_classic_button(slot_num: u8, color: Option<Color>) -> ButtonBundle {
    ButtonBundle {
        style: Style {
            position_type: PositionType::Absolute,
            position: Rect {
                top: Val::Px(70. * slot_num as f32),
                right: Val::Px(0.),
                ..default()
            },
            size: Size::new(Val::Px(60.0), Val::Px(60.0)),
            margin: Rect::all(Val::Auto),
            justify_content: JustifyContent::Center,
            align_items: AlignItems::Center,
            ..Default::default()
        },
        color: color.unwrap_or(Color::GREEN).into(),
        ..Default::default()
    }
}
