mod camera;
mod grid;
mod mouse_tracker;
mod ui;

use bevy::{input::system::exit_on_esc_system, prelude::*};
use camera::CameraPlugin;
use grid::GridPlugin;
use mouse_tracker::MouseTrackerPlugin;
use ui::UiPlugin;

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(50., 30., 50.)))
        .insert_resource(WindowDescriptor {
            width: 1280.,
            height: 720.,
            title: "Hello World".to_string(),
            resizable: false,
            scale_factor_override: Some(1.),
            ..default()
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(MouseTrackerPlugin)
        .add_plugin(GridPlugin)
        .add_plugin(CameraPlugin)
        .add_plugin(UiPlugin)
        .add_system(exit_on_esc_system)
        .run();
}
